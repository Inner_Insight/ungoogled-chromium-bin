#!/bin/bash

# Script to verify binary package on localhost 

#set -e
set -o pipefail
#set -x

# Define pubkey
GPG_PUBKEY="ungoogled-chromium.key"

if [ ! -f "${GPG_PUBKEY}" ]
then
	printf "Could not find %s\n" "${GPG_PUBKEY}"
	exit 1
fi

#printf "Downloading key.\n"
#curl <key> | gpg --import -

# Import keys
printf "Importing key.\n"
gpg --import "${GPG_PUBKEY}"

# Verify files
printf "Verify files.\n"
while read -r LINE
do
	printf "Verifying %s\n" "${LINE}"
	gpg --verify "${LINE}"
done < <(find . -iname "*.sig" -type f)
